"""
Tests for print detection.
"""

import importlib.util
spec = importlib.util.spec_from_file_location("pylint_print", "pylint_print/pylint_print.py")
pylint_print = importlib.util.module_from_spec(spec)
spec.loader.exec_module(pylint_print)

from astroid import parse
from unittest.mock import MagicMock

class TestPrintChecker():
    """
    Test cases for print function detection.
    """

    def test_print_call(self):
        module = parse('''
        print("output")
        ''')
        pp = pylint_print.PrintChecker()
        pp.add_message = MagicMock()
        pp.visit_call(module.body[-1].value)
        pp.add_message.assert_called_with('print-function', node=module.body[-1].value)

    def test_non_print_call(self):
        module = parse('''
        list()
        ''')
        pp = pylint_print.PrintChecker()
        pp.add_message = MagicMock()
        pp.visit_call(module.body[-1].value)
        pp.add_message.assert_not_called()
