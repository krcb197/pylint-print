# Pylint Print Checker

A Pylint plugin for checking for use of the print() function.

View on [PyPi](https://pypi.org/project/pylint-print/)
