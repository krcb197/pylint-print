"""pylint-print meta info."""

__title__ = 'pylint-print'
__description__ = 'Print function checker for PyLint'
__url__ = 'https://gitlab.com/nwmitchell/pylint-print'
__version__ = '1.0.0'
__author__ = 'Nick Mitchell'
__author_email__ = 'nmitchell@nvidia.com'
__license__ = 'GPLv2'
__copyright__ = 'Copyright 2021 Nick Mitchell'
